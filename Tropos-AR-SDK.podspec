Pod::Spec.new do |spec|
  spec.static_framework = true
  spec.name          = "Tropos-AR-SDK"
  spec.version       = "2.0.8"
  spec.summary       = "Tropos AR SDK"
  spec.ios.deployment_target = '13.0'
  spec.platform      = :ios, '13.0'
  spec.swift_version = '6'
  spec.description   = "Tropos Augmented Reality SDK"
  spec.homepage      =  'https://bitbucket.org/troposar/tropos-ar-ios-pod'
  spec.license       = { :type => 'MIT', :file => 'LICENSE' }
  spec.author        = { 'Tropos AR' => 'development@tropos.ar' }
  spec.source        = { :git => 'https://bitbucket.org/troposar/tropos-ar-ios-pod.git', :tag =>  spec.version.to_s }
 # spec.source_files  = '**/*.{swift}'
  spec.vendored_frameworks = 'Frameworks/AVProVideo.framework' , 'Frameworks/AVProMovieCapture.framework' , 'Frameworks/NatCorder.framework' , 'Frameworks/NatDevice.framework' , 'Frameworks/NatMLHub.framework' , 'Frameworks/TroposARSDK.framework' , 'Frameworks/UnityFramework.framework'
  spec.preserve_path = 'Frameworks/*'
  spec.dependency 'ARCore/GARSession', '~> 1.44.0'
  spec.dependency 'ARCore/Geospatial', '~> 1.44.0'
  spec.info_plist = {
     'TARIdentifier' => '[paste your Tropos AR identifier here]',
     'NSCameraUsageDescription' => 'Use camera for AR',
     'NSLocationWhenInUseUsageDescription' => 'Use GPS for AR',
     'NSMicrophoneUsageDescription' => 'Use microphone for AR',
     'NSPhotoLibraryAddUsageDescription' => 'The app requires access to Photos to save media to it.',
     'NSPhotoLibraryUsageDescription' => 'The app requires access to Photos to save media to it.'
  }
  spec.frameworks = 'AVKit', 'AVFoundation', 'CoreMotion', 'Metal', 'MetalPerformanceShaders', 'OpenAL', 'Photos', 'PhotosUI', 'SystemConfiguration'
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64', 'OTHER_LDFLAGS' => '-ObjC -ld_classic' }
  spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64', 'OTHER_LDFLAGS' => '-ObjC -ld_classic' }
  spec.subspec 'PromiseObjC' do |sp|
     sp.ios.deployment_target = '13.0'
  end  
  spec.subspec 'nanopb' do |sp|
     sp.ios.deployment_target = '13.0'
  end
  spec.subspec 'FirebaseInstallations' do |sp|
     sp.ios.deployment_target = '13.0'
  end
  spec.subspec 'FirebaseCore' do |sp|
     sp.ios.deployment_target = '13.0'
  end
  spec.subspec 'FirebaseCoreInternal' do |sp|
     sp.ios.deployment_target = '13.0'
  end
  spec.subspec 'FirebaseRemoteConfigInterop' do |sp|
     sp.ios.deployment_target = '13.0'
  end
  spec.subspec 'GoogleDataTransport' do |sp|
     sp.ios.deployment_target = '13.0'
  end
  spec.subspec 'GoogleUtilities' do |sp|
     sp.ios.deployment_target = '13.0'
  end
end
